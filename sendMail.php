﻿<?php
require 'PHPMailer/PHPMailerAutoload.php';

header('Content-Type: text/html; charset=utf-8');



$mail = new PHPMailer;

$mail->CharSet = 'UTF-8';
$mail->setFrom(strip_tags($_POST['email']), strip_tags($_POST['name']));
$mail->addAddress('kirs07@ya.ru');     // Add a recipient kirs07@ya.ru
$mail->addAddress('urist@bd-spb.ru'); 
$mail->addAddress('kd@bd-spb.ru'); 
$mail->addReplyTo(strip_tags($_POST['email']), strip_tags($_POST['name']));

foreach (json_decode($_POST['generalDirectors']) as $key => $file) {
	$newFileName = $key . "_generalDirector." . pathinfo($file, PATHINFO_EXTENSION);

	$file = str_replace("http://xn----7sbihqb7baaldln2dyh.xn----8sbbkdenhe5aqs7a.xn--p1ai/test/", '', $file);
	$file = str_replace("http://xn----7sbihqb7baaldln2dyh.xn----8sbbkdenhe5aqs7a.xn--p1ai/test", '', $file);
	if(getimagesize($file)){
		$mail->addAttachment(realpath(dirname(__FILE__)) . "/" . $file, $newFileName);

		echo $file . "--" . $newFileName;
	}
}

foreach (json_decode($_POST['uchrediteli']) as $key => $file) {
	$newFileName = $key . "_uchreditelj." . pathinfo($file, PATHINFO_EXTENSION);

	$file = str_replace("http://xn----7sbihqb7baaldln2dyh.xn----8sbbkdenhe5aqs7a.xn--p1ai/test/", '', $file);
	$file = str_replace("http://xn----7sbihqb7baaldln2dyh.xn----8sbbkdenhe5aqs7a.xn--p1ai/test", '', $file);
	if(getimagesize($file)){
		$mail->addAttachment(realpath(dirname(__FILE__)) . "/" . $file, $newFileName);

		echo $file . "--" . $newFileName;
	}
}

$mail->isHTML(true);                                  // Set email format to HTML

$mail->CharSet = 'UTF-8';
$mail->Subject = 'Письмо с сайта "регистрация-ооо.бизнес-диалог.рф"';
$mail->Body    = "Имя: " . strip_tags($_POST['name']) . "<br/>" . 
"Емайл: " . strip_tags($_POST['email']) . "<br/>" . 
"Номер телефона: " . strip_tags($_POST['phone'])  . "<br/>" . 
"Наименование ООО на русском: " . strip_tags($_POST['oooNameRus']) . "<br/>" . 
"Наименование ООО на иностранном: " . strip_tags($_POST['oooNameEng']);

if(!$mail->send()) {
    echo 'Message could not be sent.';
    echo 'Mailer Error: ' . $mail->ErrorInfo;
} else {
    echo 'Message has been sent';
}

session_destroy();
return false;