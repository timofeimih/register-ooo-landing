$(document).ready(function() {

	var scrollEvent = true;

		//examples slider
	var owl = $(".slider");
 
	owl.owlCarousel({
	 
		itemsCustom : [
			[0, 1],
			[450, 1],
			[600, 2],
			[770, 3],
			[1000, 3],
			[1200, 3],
			[1400, 3],
			[1600, 3]
		],
		navigation : true,
		navigationText : false,
	});



	$(".anchorLink").click(function(e){
		e.preventDefault();

		var elem = $(this).attr("href");
		$('html, body').animate({
	        scrollTop: $(elem).offset().top - $("#header").height()	        
	    }, 1000);
	})

	$('.go-top').click(function(e){
		e.preventDefault();

		$('html,body').animate({scrollTop:0},1000);
	});

	//fancybox
	$(".fancybox").fancybox({
		wrapCSS : 'pop-up-wrap',
		padding: 0,

		helpers: {
	        overlay: {
	            locked: true
	        }
    	}
	});
// added 20.05.16
$(".fancybox-letter-form").fancybox({
		wrapCSS : 'pop-up-wrap pop-up-wrap-letter',
		autoSize : false,
		width : 583,
		padding: 0,

		helpers: {
	        overlay: {
	            locked: true
	        }
    	}
	});




	$(".main-bn").height($(window).height() +10);

	var scrollFromTop = 1;

	$(window).scroll(function(){
		if($(window).scrollTop() > 10)
			$(".call-back").fadeIn();
		else $(".call-back").fadeOut();

		// if(scrollFromTop){
		// 	if($(window).scrollTop() < $("#first-page").height()){
		// 		$(window).stop();
		// 		$("#goToSPB").trigger("click");

				
		// 	}
		// }

		scrollFromTop = 0;

		
	})

	$("#sendForm").click(function(e){
		e.preventDefault();
		var form = $('#sendMail');

		//$(this).find("INPUT[type='submit']").attr('disabled', true);

		var generalDirectors = [];
		$("#fileupload .template-download").each(function(){
			generalDirectors.push($(this).find(".name A").attr("href"));
		});
		form.find("INPUT[name='generalDirectors']").val(JSON.stringify(generalDirectors));

		var uchrediteli = [];
		$("#fileupload2 .template-download").each(function(){
			uchrediteli.push($(this).find(".name A").attr("href"));
		});
		form.find("INPUT[name='uchrediteli']").val(JSON.stringify(uchrediteli));

		var data = form.serialize();

		var opts = {
		    url: 'sendMail.php',
		    data: data,
		    type: 'POST',
		    success: function(data){
		        form.find(".success").fadeIn();


			
				setTimeout(function(){
						$(".fancybox-close").trigger("click");
						form.trigger( 'reset' );
						form.find('.success').hide();

						$.fancybox({ 
							href: "#success_message",
							wrapCSS : 'pop-up-wrap',
							padding: 0,

							helpers: {
						        overlay: {
						            locked: true
						        }
					    	}
						});
				}, 1000)

				form.find("INPUT[type='submit']").attr('disabled', false);
		    },
		    error: function(data){
		    	alert('Сообщение не доставлено');
				form.find("INPUT[type='submit']").attr('disabled', false);
		    }
		};
		if(data.fake) {
		    // Make sure no text encoding stuff is done by xhr
		    opts.xhr = function() { var xhr = $.ajaxSettings.xhr(); xhr.send = xhr.sendAsBinary; return xhr; }
		    opts.contentType = "multipart/form-data; boundary="+data.boundary;
		    opts.data = data.toString();
		}
		$.ajax(opts);


	})

	$(".call-form .phone").mask("+7 (999) 999-99-99", {placeholder: "+7"});
	$(".sendMail .phone").mask("+7 (999) 999-99-99", {placeholder: "+7"});

	$('.call-form').submit(function(e){
		e.preventDefault();
		$("#exten").val($(this).find(".phone").val());

		$("#calls-me-btn").trigger("click");

		$(this).find("INPUT").attr("disabled", true);
		
		var form = $(this);

		setTimeout(function(){
			form.find("INPUT").attr("disabled", false);
		}, 8000);
	})

	$(".table-wrap").hover(function(){
		$(this).find(".hideShow").show();
		$(this).find('.icon-check').hide();
	}, function(){
		$(this).find(".hideShow").hide();
		$(this).find('.icon-check').show();
	})

	$(".addAdditional").click(function(){
		var additional = $(".additional").last();
		additional.after($(".toClone.additional").clone());
		
		//новый последний элемент
		additional = $(".additional").last();
		additional.removeClass("toClone");

		var count = $(".additional").length - 1;
		additional.find(".count").html(count);
		additional.find("INPUT").each(function(){
			var newName = $(this).attr("name").replace(/[0-9]/g, count);
			$(this).attr("name", newName);
		})


	})

	$(".deleteLastAdditional").click(function(){
		if($(".additional").length > 2){
			$(".additional").last().remove();
		}
	})

});
