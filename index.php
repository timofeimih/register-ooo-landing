﻿<!doctype html>
<html class="no-js" lang="">
	<head>
		<meta charset="utf-8">
		<title>Регистрация 000</title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=0.5">
		<link href="favicon.ico" rel="shortcut icon" type="image/x-icon" />

		<link rel="stylesheet" href="css/normalize.min.css">
		<!--<link rel="stylesheet/less" type="text/css" href="css/main.less">-->

		<link rel="stylesheet" type="text/css" href="css/call.css">
		<link rel="stylesheet" type="text/css" href="css/main.css">
		

		
		<link rel="stylesheet" type="text/css" href="css/jquery.fancybox.css">
		<link rel="stylesheet" type="text/css" href="css/jquery.formstyler.css">
		<link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
			

		<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
		<link rel="stylesheet" href="jquery-file-upload/css/style.css">
		<!-- blueimp Gallery styles -->
		<link rel="stylesheet" href="//blueimp.github.io/Gallery/css/blueimp-gallery.min.css">
		<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
		<link rel="stylesheet" href="jquery-file-upload/css/jquery.fileupload.css">
		<link rel="stylesheet" href="jquery-file-upload/css/jquery.fileupload-ui.css">
		<!-- CSS adjustments for browsers with JavaScript disabled -->
		<noscript><link rel="stylesheet" href="jquery-file-upload/css/jquery.fileupload-noscript.css"></noscript>
		<noscript><link rel="stylesheet" href="jquery-file-upload/css/jquery.fileupload-ui-noscript.css"></noscript>
		<!--<script type="text/javascript" src="js/vendor/less.js"></script>-->

		

		<!--[if lt IE 9]>
			<script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
			<script>window.html5 || document.write('<script src="js/vendor/html5shiv.js"><\/script>')</script>
		<![endif]-->
	</head>
	<body>
		<!--[if lt IE 8]>
			<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
		<![endif]-->

	
		<header id="header">
			<div class="main-container clearfix">
				<a href="#first-page" class="logo anchorLink">
					<div></div>
					<span>Бизнес Диалог</span>
				</a>

				<div class="nav-menu">
					<ul class="claerfix">
						<li>
							<a href="#цены" class="anchorLink">
								<i class="icon-price"></i>
								СТОИМОСТЬ
							</a>
						</li>

						<li>
							<a href="#отзывы" class="anchorLink">
								<i class="icon-comments"></i>
								ОТЗЫВЫ
							</a>
						</li>

						<li class="mail">
							<a class="fancybox-letter-form" href="#pop-up">
								<i class="icon-mail"></i>
								ПОЛЕТЕЛИ С НАМИ?
							</a>
						
						</li>

						
					</ul>

					
				</div>

				<div class="phone">
					<p>
						<i class="icon-phone_ico"></i>
						+7 (812) 448-64-44
						<span class="small-phon">8 800 500 05 92</span>
					</p>
					<!-- <div class='small'>Звоните нам легко 24/7</div>
					<div class='big red-text'>+7 (812) 44-86-444</div>
					<div class='small'>8 800 500 05 92 (Бесплатный звонок из регионов России)</div> -->
				
				</div>

			</div>

		</header><!-- end#header -->

		<div id="main">
			<div id="first-page">
				<div class="wrap">
					<div class="main-container clearfix">

						<div class="left-fixed">
							<div class="registration">
								<span class="gray-text">Полет начинается со взлета,</span>
								<span class="red-text">а бизнес с регистрации</span>						
							</div>

							<div class="logo-color"></div>
							<div class="line"></div>

							<div class="ball-wrap">
								<div class="ball">
<br/>									
<span class="reg-text">ДОСТАВИМ В МИР БИЗНЕСА ЗА<br/></span><br/>
									<span class="for"></span>
									<span class="terms">
										<span class="white-bg-text">3 ДНЯ</span> и <span class="white-bg-text">8 МИНУТ</span>
									</span>
								</div>
							</div>						
						</div>

						<div class="moovable">
							<div class="small-stars"></div>
		 					<div class="cloud_1" ></div>
							<div class="cloud_2"></div>
							<div class="cloud_3"></div>

							<div class="rocketman"></div>
						</div>
						<a href="#spb" class="mouse anchorLink" id='goToSPB'></a>
					</div>
				</div>
			</div> <!-- .first-page END -->	

			<div id="spb">
				<div class="spb-logo"></div>
				<div class="deco"></div>

				<ul class="spb-list">
					<li>
						<span class="icon-22min"></span>
						<span class="circle-small"></span>
						Делаем документы<br/>
						без ошибок<br/>
						за 8 минут
					</li>
					<li>
						<span class="icon-money"></span>
						<span class="circle-small"></span>
						Не берем денег за<br/>
						срочность и<br/>
						сложность
					</li>
					<li>
						<span class="icon-web"></span>
						<span class="circle-small"></span>
						Готовим документы<br/>
						быстрее любого<br/>
						on line сервиса
					</li>
					<li>
						<span class="icon-diploma"></span>
						<span class="circle-small"></span>
						Мы не стартап!<br/>
						Регистрируем ООО<br/>
						с 2009 года
					</li>
				</ul>

				<a href="#advantages" class="mouse-circle anchorLink"></a>
			</div>

			<div id="advantages">
				<div class="main-container">
					<h1 class="adv-title">С НАМИ ВЫГОДНО, УДОБНО, БЫСТРО И ЛЕГКО</h1>

					<div class="list-wrap">
						<ul class="advantage-list clearfix">
							<li>
								<div class="img-block">
									<span class="icon-map"></span>
								</div>
								<div class="description">Откроем для Вас</br>офис даже в выходной день</div>
							</li>
							<li>
								<div class="img-block">
									<span class="year"></span>
								</div>
								<div class="description">Не беспокойтесь за сроки,<br/>за всем следят наши роботы</div>
							</li>
							<li>
								<div class="img-block">
									<span class="happy"></span>
								</div>
								<div class="description">В тёплых отношениях<br/>с Нотариусами Санкт-Петербурга</div>
							</li>
							<li>
								<div class="img-block">
									<span class="stacked"></span>
								</div>
								<div class="description">Возьмем на себя весь процесс<br/>Используйте свободное время с пользой</div>
							</li>
							<li>
								<div class="img-block">
									<span class="back"></span>
								</div>
								<div class="description">Вернем деньги, если ООО<br/>не зарегистрируют по нашей вине</div>
							</li>
							<li>
								<div class="img-block">
									<span class="Renessans"></span>
								</div>
								<div class="description">Ваш заказ будет застрахован<br/>в СК “Ренессанс”. Вы ничем не рискуете</div>
							</li>
						</ul>
					</div>
				</div>	
			</div>
			
			<div id='отзывы'></div>
			<div id="reviews">
				<div class="main-container">
					<h1 class="rew-title">ОФИЦИАЛЬНЫЕ РЕКОМЕНДАЦИИ НАШИХ КЛИЕНТОВ</h1>
					<div class="slider-wrap">
						<div class="owl-carousel owl-theme slider">
<div class="item">
                                <div class="img-block">
                                    <div class="hover-block">
                                        <a href="img/otzqvi/istok.png" title='ООО «Исток»' data-fancybox-group="group1" class="fancybox"><i class="icozoom"><i></i></i></a>
                                    </div>

                                    <div class='wrapper'><img src="img/otzqvi/istok_thumb.png" alt="ООО «Исток»"></div>
                                </div>

                                <p class="caption">ООО «Исток»</p>
                            </div> 
<div class="item">
                                <div class="img-block">
                                    <div class="hover-block">
                                        <a href="img/otzqvi/Император.jpg" title='ООО «Император»' data-fancybox-group="group1" class="fancybox"><i class="icozoom"><i></i></i></a>
                                    </div>

                                    <div class='wrapper'><img src="img/otzqvi/Император_thumb.jpg" alt="ООО «Император»"></div>
                                </div>

                                <p class="caption">ООО «Император»</p>
                            </div>
<div class="item">
                                <div class="img-block">
                                    <div class="hover-block">
                                        <a href="img/otzqvi/Межрегионэнергострой.jpg" title='ООО «Межрегионэнергострой»' data-fancybox-group="group1" class="fancybox"><i class="icozoom"><i></i></i></a>
                                    </div>

                                    <div class='wrapper'><img src="img/otzqvi/Межрегионэнергострой_thumb.jpg" alt="ООО «Межрегионэнергострой»"></div>
                                </div>

                                <p class="caption">ООО «Межрегионэнергострой»</p>
                            </div>
							<div class="item">
                                <div class="img-block">
                                    <div class="hover-block">
                                        <a href="img/otzqvi/Армо СПб-page-001.jpg"  title='ООО «АРМО-СПБ»' data-fancybox-group="group1" class="fancybox"><i class="icozoom"><i></i></i></a>
                                    </div>

                                    <div class='wrapper'><img src="img/otzqvi/Армо СПб-page-001_thumb.jpg" alt="ООО «АРМО-СПБ»"></div>
                                </div>

                                <p class="caption">ООО «АРМО-СПБ»</p>
                            </div>

                            <div class="item">
                                <div class="img-block">
                                    <div class="hover-block">
                                        <a href="img/otzqvi/Севериконд-page-001.jpg" title='ООО «Севереконд»'  data-fancybox-group="group1" class="fancybox"><i class="icozoom"><i></i></i></a>
                                    </div>

                                    <div class='wrapper'><img src="img/otzqvi/Севериконд-page-001_thumb.jpg" alt="ООО «Севереконд»"></div>
                                </div>

                                <p class="caption">ООО «Севериконд»</p>
                            </div>

                            <div class="item">
                                <div class="img-block">
                                    <div class="hover-block">
                                        <a href="img/otzqvi/Переводчикофф.JPG" title='Бюро переводов «Переводчикофф»'  data-fancybox-group="group1" class="fancybox"><i class="icozoom"><i></i></i></a>
                                    </div>

                                    <div class='wrapper'><img src="img/otzqvi/Переводчикофф_thumb.JPG" alt="Бюро переводов «Переводчикофф»"></div>
                                </div>

                                <p class="caption">Бюро переводов «Переводчикофф»</p>
                            </div>

                            <div class="item">
                                <div class="img-block">
                                    <div class="hover-block">
                                        <a href="img/otzqvi/ФДС-page-001.jpg"  title='ООО «ФДС»' data-fancybox-group="group1" class="fancybox"><i class="icozoom"><i></i></i></a>
                                    </div>

                                    <div class='wrapper'><img src="img/otzqvi/ФДС-page-001_thumb.jpg" alt="ООО «ФДС»"></div>
                                </div>

                                <p class="caption">ООО ФДС</p>
                            </div>

                            <div class="item">
                                <div class="img-block">
                                    <div class="hover-block">
                                        <a href="img/otzqvi/Ланга СП.jpg" title='ООО «ЛАНГА-СП»' data-fancybox-group="group1" class="fancybox"><i class="icozoom"><i></i></i></a>
                                    </div>

                                    <div class='wrapper'><img src="img/otzqvi/Ланга СП_thumb.jpg" alt="ООО «ЛАНГА-СП»"></div>
                                </div>

                                <p class="caption">ООО «ЛАНГА-СП»</p>
                            </div>

                            <div class="item">
                                <div class="img-block">
                                    <div class="hover-block">
                                        <a href="img/otzqvi/ОООЭкорусметал.jpg" title='ООО «Экорусметалл»' data-fancybox-group="group1" class="fancybox"><i class="icozoom"><i></i></i></a>
                                    </div>

                                    <div class='wrapper'><img src="img/otzqvi/ОООЭкорусметал_thumb.jpg" alt="ООО «Экорусметалл»"></div>
                                </div>

                                <p class="caption">ООО «Экорусметалл»</p>
                            </div>

                            

                            <div class="item">
                                <div class="img-block">
                                    <div class="hover-block">
                                        <a href="img/otzqvi/mobilnqe_tehnologii.png" title='ООО «Мобильные технологии»' data-fancybox-group="group1" class="fancybox"><i class="icozoom"><i></i></i></a>
                                    </div>

                                    <div class='wrapper'><img src="img/otzqvi/mobilnqe_tehnologii_thumb.png" alt="ООО «Мобильные технологии»"></div>
                                </div>

                                <p class="caption">ООО «Мобильные технологии»</p>
                            </div>

                        

						</div>							
					</div> <!-- .slider-wrap END -->

					<div class="more-reviews">
						<h3 class="more-title">ЧИТАЙТЕ ОТЗЫВЫ О НАШЕЙ РАБОТЕ НА НЕЗАВИСИМЫХ ПОРТАЛАХ</h3>
						<br/>

						<ul class="link-list">
							<li>
								<a href="http://www.yp.ru/spb/search/text/%D0%B1%D0%B8%D0%B7%D0%BD%D0%B5%D1%81+%D0%B4%D0%B8%D0%B0%D0%BB%D0%BE%D0%B3/ch_id/2919152/" target="_blank">yp.ru</a>
							</li>
							<li>
								<a href="https://www.google.ru/?gws_rd=ssl#newwindow=1&amp;q=%D0%B1%D0%B8%D0%B7%D0%BD%D0%B5%D1%81+%D0%B4%D0%B8%D0%B0%D0%BB%D0%BE%D0%B3&amp;lrd=0x4696305a2bd244b1:0xfb87bae878e64949,1" target="_blank">google.com</a>
							</li>
							<li>
								<a href="https://maps.yandex.ru/2/saint-petersburg/?source=serp_navig&amp;text=bd-spb.ru&amp;sll=28.092189%2C59.359847&amp;sspn=5.229492%2C1.382663&amp;ll=30.376441%2C59.951776&amp;z=11" target="_blank">maps.yandex.ru</a>
							</li>
							<li>
								<a href="http://spb.tulp.ru/yuridicheskie-kompanii/yuridicheskaya-kompaniya-biznes-dialog" target="_blank">spb.tulp.ru</a>
							</li>
							<li>
								<a href="http://adres-spb.ru/view.php?id=103644" target="_blank">adres-spb.ru</a>
							</li>
						</ul>
					</div>
				</div>
			</div>

			

			<div id="steps">
				<div class="main-container">
					<h2 class="steps-title">БИЗНЕС ДИАЛОГ - САМЫЙ БЫСТРЫЙ РЕГИСТРАТОР ООО</h2>
					<div class="steps-list">
						<div class="inline-item">
							<span class="min8"></span>
							<div class="description">
								Подготовка документов<br/>
для регистрации ООО<br/>
							</div>
						</div>
						<span class="plus"></span>
						<div class="inline-item">
							<span class="days6"></span>
							<div class="description">Регистрация в налоговой<br/>
инспекции</div>
						</div>
						<span class="arrow-down"></span>
						<div class="item">
							<span class="full-term"><span class="wow"></span></span>							
						</div>
						<div class="item">
							<span class="white-logo"></span>
						</div>
					</div>
				</div>
			</div>

			<div id="required">
				<div class="main-container">
					<div class="req-title">ОТ ВАС ТОЛЬКО</div>
					<div class="required-list">

						<div class="item">
							<div class="img-block passport"></div>
							<span class="circle-blue"></span>
							<div class="text-block blue-block">
								<h4>
</br>1. Паспорт учредителя и руководителя</h4>
А так же номер ИНН. Вы не получали номер ИНН? Не беспокойтесь, налоговая его присвоит автоматически. Никаких дополнительных действий не потребуется. Это бесплатно.
							</div>
						</div>

						<div class="item">
							<div class="img-block OOOname"></div>
							<span class="circle-gray"></span>
							<div class="text-block gray-block">
								<h4>
</br>2. Наименование ООО</h4>
								Как корабль назовешь, так он и поплывет. Позаботьтесь о наименованиее компании на иностранном языке,  если ваши заказчики или партнеры - резиденты иностранных государств
							</div>
						</div>

					</div>
				</div>				
			</div>

			<div id="prices">
				<div class="main-container">
					<div id="цены">
					
					</div>
					
				
					<div class="table-wrap">
						<ul class="table-header">
						<li >СТОИМОСТЬ</br>РЕГИСТРАЦИИ ООО</li>
						<li>Я сам(а)</li>
						<li>Лучше Вы</li>
						</ul>
						<div class="wrap">
							<ul class="table-row" style='height:100px'>
								<li>Государственная пошлина</li>
								<li><span class="icon-check"></span> <article class='hideShow price-item' style='display:none;'>4 000<span class="rub">o</span><br/><span class="small-text" style='margin:0'>Оплачу сам(а)</span></article></li>
								<li><span class="icon-check"></span> <article class='hideShow price-item' style='display:none'>4 000<span class="rub">o</span></article></li>
							</ul>
						</div>

						<div class="wrap">
							<ul class="table-row">
								<li>Подготовка документов для регистрации ООО</li>
								<li><span class="icon-check"></span> <article class='hideShow' style='display:none'>1 900<span class="rub">o</span></article></li>
								<li><span class="icon-check"></span> <article class='hideShow' style='display:none'>1 900<span class="rub">o</span></article></li>
							</ul>
						</div>

						<div class="wrap">
							<ul class="table-row">
								<li>Подача и получение в налоговый орган</li>
								<li><span class="small-text" style='margin:0'>Я сам(а)</span></li>
								<li><span class="icon-check"></span> <article class='hideShow' style='display:none'>1 000<span class="rub">o</span></article></li>
							</ul>
						</div>
						<ul class="table-row last-row">
							<li>Нотариальные расходы на 1 учредителя</li>
							<li><span class="small-text" style='margin:0'>Нотариус не<br/>требуется</span></li>
							<li><span class="icon-check"></span> <article class='hideShow' style='display:none'>1 850<span class="rub">o</span></article></li>
						</ul>

						<ul class="table-bottom">
							<li></li>
							<li>5 900<span class="rub">o</span></li>
							<li>8 750<span class="rub">o</span></li>
						</ul>
					</div>

					<div class="atention">
					<div class="ribbons"></div>
					<div class="stars"></div>
					<h2 class="red-title">ВНИМАНИЕ!</h2>
					<h3 class="sub-title">НАШЕ ПРЕДЛОЖЕНИЕ ДЕЙСТВИТЕЛЬНО:</h3>
						<div class="timer">
							<div class="counter">
								<p class="timer-text left">ЕЩЁ</p>
								<span>
									<p class="timer-item">0</p>
									<p class="timer-item">2</p>
								</span>
								<p class="timer-text right">ДНЯ</p>
							</div>
						</div>
					</div>
				</div>
			</div>


			<div id="services">
				<div class="main-container">
					<h2 class="service-title">ВСЕ УСЛУГИ ДЛЯ БИЗНЕСА В ОДНОМ МЕСТЕ</h2>

					<div class="wrap">
						<div class="inner-wrap">
							<ul class="service-list clearfix">

								<li>
									<div class="img-block">
										<span class="bank"></span>
									</div>
									<div class="description-red">
										<p><b>Открытие расчетных счетов</b></p>
										<p>в Банках Санкт-Петербурга</p>
										<span class="cost">от 2 500<span class="rub">o</span></span>
									</div>
								</li>

								<li>
									<div class="img-block">
										<span class="point"></span>
									</div>
									<div class="description-blue">
										<p><b>Юридические адреса</b></p>
										<p>с почтовым обслуживанием и без</p>
										<span class="cost">от 3 200<span class="rub">o</span>/год</span>
									</div>
								</li>

								<li>
									<div class="img-block">
										<span class="tie"></span>
									</div>
									<div class="description-blue">
										<p><b>Бухгалтерские услуги для ООО</b></p>
										<p>Учет от А до Я</p>									
										<span class="cost">от 660<span class="rub">o</span>/мес</span>
									</div>
								</li>

								<li>
									<div class="img-block">
										<span class="stamp"></span>
									</div>
									<div class="description-red">
										<p><b>Изготовление печатей,</b></p>
										<p>штампов и визиток</p>
										<span class="cost">от 350<span class="rub">o</span></span>
									</div>
								</li>

								<li>
									<div class="img-block">
										<span class="cloud_red"></span>
									</div>
									<div class="description-red">
										<p><b>Выписки из ЕГРЮЛ</b></p>
										<p>Срочные и не очень</p>									
										<span class="cost">от 1 190<span class="rub">o</span></span>
									</div>
								</li>

								<li>
									<div class="img-block">
										<span class="rent"></span>
									</div>
									<div class="description-blue">
										<p><b>Аренда программ линейки 1С</b></p>
										<p>И серверов</p>								
										<span class="cost">от 790<span class="rub">o</span></span>
									</div>
							</li>
							</ul>	
					</div>	
				</div>
</div>

				<div id="withUs">
					<div class="main-container">
						<div class="wrap"><br/>
							<ul class="client-list"><br/><br/><br/><br/>
<h2 class="service-title">НАС ВЫБИРАЮТ ЛУЧШИЕ</h2><br/>
								<li><span class="Life"></span></li>
								<li><span class="CCP"></span></li>
								<li><span class="galaktika"></span></li>
								<li><span class="imperator"></span></li>
								<li><span class="petro"></span></li><br/><br/><br/><br/><br/>
							</ul>
						</div>
					</div>
				</div>

				<div class='main-container'>
					<h2 class="service-title">МЫ УЖЕ ВЫБРАЛИ ДЛЯ ВАС БАНКИ. ТЕПЕРЬ ВЫБОР ЗА ВАМИ</h2>
					<ul class="partners-list">
						<li>
					
							
							<a href='img/alfa.png' rel='sertivicats' class="fancybox bank-item">
								<img src="img/alfa.jpg" title="Альфа-Банк" alt="Альфа-Банк">
							</a> 
							<p class="caption">Комиссия Банка: 0 руб</p>
							<p class="caption">Тарифы от: 1 290 руб/мес</p>
							<p class="caption">Бонус 12 000 р на Яндекс.Директ</p>            
						 </li>
						<li>
							
							
							<a href='img/open.png' rel='sertivicats' class="fancybox bank-item">
								<img src="img/otkritie.jpg" title="Банк-Открытие" alt="Банк-Открытие">
							</a>
							
							<p class="caption">Комиссия Банка: 0 руб</p>
							<p class="caption">Тарифы от: 850 руб/мес</p>               
						 </li>
						<li>
							
							
							<a href='img/rajffajzen.png' rel='sertivicats' class="fancybox bank-item">
								<img src="img/rajffajzen_thumb.png" title="Райффайзен-Банк" alt="Райффайзен-Банк">
							</a>
							
							<p class="caption">Комиссия Банка: 0 руб</p>
							<p class="caption">Тарифы от: 880 руб/мес</p>   
						    <p class="caption">Бонус 2000 р на Google Adwords</p>               
						 </li>
						<li>
							
							
							<a href='img/tochka.png' rel='sertivicats' class="fancybox bank-item">
								<img src="img/tochka_thumb.png" title="Точка-Банк" alt="Точка-Банк">
							</a>
					
							<p class="caption">Комиссия Банка: 0 руб</p>
							<p class="caption">Тарифы от: 750 руб/мес</p>                
						</li>						
					</ul>
				</div>
			</div>

			<div class="map" id='offisq'><div class='map_title'><h2 class="service-title" style="text-align: center;">САМАЯ БЛИЗКАЯ СЕТЬ ЮРИДИЧЕСКИХ ОФИСОВ</h2></div><script type="text/javascript" charset="utf-8" src="https://api-maps.yandex.ru/services/constructor/1.0/js/?sid=xQRiyijVt3OFaqeU6wo0UYS3a1kiCoZ9&width=100%&height=100%&lang=ru_RU&sourceType=constructor"></script></div>

		</div> <!-- #main END -->


		<footer id="footer">
			<div class="main-container">
				<a href="#" class="go-top"><i></i></a>
				
				<div class="contacts clearfix">
					<div class="footer-col vk-col">
						<a href="https://vk.com/busdialogue" target='_blank'>
							<i class="icon-vk_footer"></i>
						</a>
					</div>
					
					<div class="footer-col">
						<p>
							<i class="icon-phone_ico"></i>
							+7 (812) 448-64-44
							<span class="small-phon">8 800 500 05 92</span>
						</p>
					</div>
					
					<div class="footer-col">
						<a href='mail:k@bd-spb.ru' >
							<i class="icon-site_footer"></i>k@bd-spb.ru<i class="icon-vk_footer mob-vk"></i>
						</a>
					</div>
					
					<a href="http://бизнес-диалог.рф"  target='_blank' class="logo-footer">
						<i class="icon-Logo_grey"></i>
					</a>
				</div>

				<div class="copy-lang">
					<p>ООО “ЮФ” Бизнес Диалог”, сеть офисов юридическо-бухгалтерского сопровождения малого и среднего бизнеса. ОГРН1097847093850, ИНН7810550277</p>
				</div>
			</div>

			<div class="call-back">
				<div class="main-container">
					<p>Хотите, позвоним Вам через 25 секунд?</p>
					<form action="" class='call-form'>
						<input type="text" name='phone' class='phone' placeholder="+7" disabled>
						<input type="submit" value='заказать' class='btn btn-call-back' disabled>
					</form>
				</div>
			</div>
		</footer>

		<div id='success_message' class='fancybox'><p class='success'>Сообщение отправлено. Все отлично! Мы скоро Вам ответим.</p></div>

		<div id="pop-up" class="question-pop-up">
			<form action="/sendMail.php" id='sendMail'>
	        	<h3 class="title-pop-up">Отправить письмо</h3>
	        	<p class='success' style='display: none'>Сообщение отправлено. Все отлично! Мы скоро Вам ответим.</p>
		        <div class="input-container">
		            <label class="caption">Как Вас зовут?</label>
		            <input type="text" name='name'>
		            <label class="error-caption">Имя введенo неверно!</label>
		        </div>

		        <div class="input-container">
		            <label class="caption">Ваш e-mail</label>
		            <input type="email" required placeholder="example@example.ru" name='email'>
		            <label class="error-caption">E-mail адрес введен неверно!</label>
		        </div>

		        <div class="input-container">
		            <label class="caption">Ваш мобильный телефон</label>
		            <input type="text" required class='phone' name='phone' placeholder="+7">
		        </div>

				<div class="input-container">
		            <label class="caption">Наименование ООО на русском</label>
		            <input type="text" name='oooNameRus'>
		        </div>

		        <div class="input-container">
		            <label class="caption">Наименование ООО на иностранном</label>
		            <input type="text" name='oooNameRus'>
		        </div>

				<input type="hidden" name='generalDirectors'/>
				<input type="hidden" name='uchrediteli'/>
		        
	        </form>

	        <div id="addImages">
				
				<div class="image-block">
					<h3 class="text-center">Добавить паспорт Генерального директора</h3>
					<!-- changes 20.05.16 -->
					<h4>Сканы паспорта:</h4>
					<p>Разворот с фото, разворот с пропиской</p>
					<!-- changes 20.05.16 ENDS -->


		        	<!-- The file upload form used as target for the file upload widget -->
			    	<form id="fileupload" action="//jquery-file-upload.appspot.com/" method="POST" enctype="multipart/form-data">
				        <!-- Redirect browsers with JavaScript disabled to the origin page -->
				        <noscript><input type="hidden" name="redirect" value="https://blueimp.github.io/jQuery-File-Upload/"></noscript>
				        <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
				        <div class="row fileupload-buttonbar">
				        				<!-- changes 20.05.16 -->
												<!-- buttons using bootstrap classes -->
												<!-- remove glyphicon on buttons -->
												<!-- remove checkbox -->
												<!-- changes button text -->
												<!-- aligned buttons using bootstrap classes -->
												<!-- added class to the table -->
												<!-- changes 20.05.16 ENDS -->
				            <div class="file_button_holder text-right">
				                <!-- The fileinput-button span is used to style the file input field as button -->											
												
				                <span class="btn btn-success btn-sm fileinput-button">
				                    <span>Добавить файлы</span>
				                    <input type="file" name="files[]" multiple>
				                </span>
				                <button type="submit" class="btn btn-primary btn-sm start">
				                    <span>Загрузить</span>
				                </button>
				                <!-- changes 20.05.16 ENDS -->

				                <!-- The global file processing state -->
				                <span class="fileupload-process"></span>
				            </div>
				            <!-- The global progress state -->
				            <div class="col-lg-12 fileupload-progress fade">
				                <!-- The global progress bar -->
				                <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
				                    <div class="progress-bar progress-bar-success" style="width:0%;"></div>
				                </div>
				                <!-- The extended global progress state -->
				                <div class="progress-extended">&nbsp;</div>
				            </div>
				        </div>
				        <!-- The table listing the files available for upload/download -->
				        <table role="presentation" class="table pop-up-wrap-letter-table"><tbody class="files"></tbody></table>
 				        <div id="dropzone1" class="dropzone">
				        	<div class="dropzone-visual">
				        		<img src="img/drag.png" alt="">
				        	</div>
				        	<div class="dropsone-descr">Добавьте файлы<br>или перетяните сюда.</div>
				        </div>
			    	</form>
				</div>

				<div class="image-block">
					<h3 class="text-center">Добавить паспорта учредителей</h3>
					<!-- changes 20.05.16 -->
					<h4>Добавьте сканы паспорта всех учредителей:</h4>
					<p>Разворот с фото, разворот с пропиской</p>
					<!-- changes 20.05.16 ENDS -->


		        	<!-- The file upload form used as target for the file upload widget -->
			    	<form id="fileupload2" action="//jquery-file-upload.appspot.com/" method="POST" enctype="multipart/form-data">
				        <!-- Redirect browsers with JavaScript disabled to the origin page -->
				        <noscript><input type="hidden" name="redirect" value="https://blueimp.github.io/jQuery-File-Upload/"></noscript>
				        <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
				        <div class="row fileupload-buttonbar">
				        				<!-- changes 20.05.16 -->
												<!-- buttons using bootstrap classed -->
												<!-- remove glyphicon on buttons -->
												<!-- remove checkbox -->
												<!-- changes button text -->
												<!-- aligned buttons using bootstrap classes -->
												<!-- added class to the table -->
												<!-- changes 20.05.16 ENDS -->
				            <div class="file_button_holder text-right">
				                <!-- The fileinput-button span is used to style the file input field as button -->

				                <span class="btn btn-success btn-sm fileinput-button">
				                    <span>Добавить файлы</span>
				                    <input type="file" name="files[]" multiple>
				                </span>
				                <button type="submit" class="btn btn-primary btn-sm start">
				                    <span>Загрузить</span>
				                </button>
				                <!-- changes 20.05.16 ENDS -->				               
				                <!-- The global file processing state -->
				                <span class="fileupload-process"></span>
				            </div>
				            <!-- The global progress state -->
				            <div class="col-lg-12 fileupload-progress fade">
				                <!-- The global progress bar -->
				                <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
				                    <div class="progress-bar progress-bar-success" style="width:0%;"></div>
				                </div>
				                <!-- The extended global progress state -->
				                <div class="progress-extended">&nbsp;</div>
				            </div>
				        </div>
				        <!-- The table listing the files available for upload/download -->
				        <table role="presentation" class="table pop-up-wrap-letter-table"><tbody class="files"></tbody></table>
				        <div id="dropzone2" class="dropzone">
				        	<div class="dropzone-visual">
				        		<img src="img/drag.png" alt="">
				        	</div>
				        	<div class="dropsone-descr">Добавьте файлы<br>или перетяните сюда.</div>
				        </div>
			    	</form>
				</div>
	        	
	        </div>

	        

		    <div class="input-container align-center red-btn-container" style='overflow:auto'>
		    			<!-- changes 26.05.16 -->
		    			<!-- add btn-block class -->
	            <button id='sendForm' class="btn btn-red btn-block">ОТПРАВИТЬ ПИСЬМО</button>
	        </div>
	    </div>

	    <script src="js/vendor/jquery-2.1.4.min.js"></script>
		<script src="js/vendor/owl.carousel.min.js" type="text/javascript"></script>
		<script src="js/vendor/jquery.fancybox.pack.js" type="text/javascript"></script>
		<script src="js/vendor/jquery.formstyler.min.js" type="text/javascript"></script>		
		<script type="text/javascript" src="js/vendor/fontsmoothie.min.js" async></script>
		<script src="js/vendor/jquery.mask.js"></script>

		<script src="js/main.js"></script>

		<!-- The template to display files available for upload -->
		<!-- changes 20.05.2016 -->
		<!-- enlarge buttons using bootstrap classes -->
		<!-- remove glyphicon on buttons -->
		<!-- change the ammount of td in table -->
		<!-- change button text eng to rus -->
		<!-- change button text: Отменить to Удалить, NOT functionality -->
		<!-- removed progress bar -->
		<!-- changes 20.05.2016 ENDS -->
		<script id="template-upload" type="text/x-tmpl">
		{% for (var i=0, file; file=o.files[i]; i++) { %}
		    <tr class="template-upload fade">
		        <td>
		            <span class="preview"></span>
		        </td>
		        <td>
		            <p class="name">{%=file.name%}</p>
		            <strong class="error text-danger"></strong>
		            <p class="size">Processing...</p>		            
		        </td>
		        <td class="text-right">
		            {% if (!i && !o.options.autoUpload) { %}
		                <button class="btn btn-primary btn-sm start" disabled>
		                    <span>Загрузить</span>
		                </button>
		            {% } %}
		            {% if (!i) { %}
		                <button class="btn btn-danger btn-sm cancel">
		                    <span>Удалить</span>
		                </button>
		            {% } %}
		        </td>
		    </tr>
		{% } %}
		</script>
		<!-- The template to display files available for download -->
		<!-- changes 20.05.2016 -->
		<!-- enlarge buttons using bootstrap classes -->
		<!-- remove glyphicon on buttons -->
		<!-- remove checkbox -->
		<!-- change the ammount of td in table -->
		<!-- removed progress bar -->
		<!-- changes 20.05.2016 ENDS -->		
		<script id="template-download" type="text/x-tmpl">
		{% for (var i=0, file; file=o.files[i]; i++) { %}
		    <tr class="template-download fade">
		        <td>
		            <span class="preview">
		                {% if (file.thumbnailUrl) { %}
		                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}"></a>
		                {% } %}
		            </span>
		        </td>
		        <td>
		            <p class="name">
		                {% if (file.url) { %}
	                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
		                {% } else { %}
		                    <span>{%=file.name%}</span>
		                {% } %}
		            </p>
		            {% if (file.error) { %}
		                <div><span class="label label-danger">Error</span> {%=file.error%}</div>
		            {% } %}
		            <span class="size">{%=o.formatFileSize(file.size)%}</span>
		        </td>
		        <td class="text-right">
		            {% if (file.deleteUrl) { %}
		                <button class="btn btn-danger btn-sm delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
		                    <span>Удалить</span>
		                </button>		                
		            {% } else { %}
		                <button class="btn btn-warning btn-sm cancel">
		                    <span>Отменить</span>
		                </button>
		            {% } %}
		        </td>
		    </tr>
		{% } %}
		</script>

		<script src="jquery-file-upload/js/vendor/jquery.ui.widget.js"></script>
		<!-- The Templates plugin is included to render the upload/download listings -->
		<script src="//blueimp.github.io/JavaScript-Templates/js/tmpl.min.js"></script>
		<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
		<script src="//blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script>
		<!-- The Canvas to Blob plugin is included for image resizing functionality -->
		<script src="//blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
		<!-- Bootstrap JS is not required, but included for the responsive demo navigation -->
		<script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
		<!-- blueimp Gallery script -->
		<script src="//blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js"></script>
		<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
		<script src="jquery-file-upload/js/jquery.iframe-transport.js"></script>
		<!-- The basic File Upload plugin -->
		<script src="jquery-file-upload/js/jquery.fileupload.js"></script>
		<!-- The File Upload processing plugin -->
		<script src="jquery-file-upload/js/jquery.fileupload-process.js"></script>
		<!-- The File Upload image preview & resize plugin -->
		<script src="jquery-file-upload/js/jquery.fileupload-image.js"></script>
		<!-- The File Upload audio preview plugin -->
		<script src="jquery-file-upload/js/jquery.fileupload-audio.js"></script>
		<!-- The File Upload video preview plugin -->
		<script src="jquery-file-upload/js/jquery.fileupload-video.js"></script>
		<!-- The File Upload validation plugin -->
		<script src="jquery-file-upload/js/jquery.fileupload-validate.js"></script>
		<!-- The File Upload user interface plugin -->
		<script src="jquery-file-upload/js/jquery.fileupload-ui.js"></script>
		<!-- The main application script -->
		<script src="jquery-file-upload/js/main.js"></script>
		<!-- The XDomainRequest Transport is included for cross-domain file deletion for IE 8 and IE 9 -->
		<!--[if (gte IE 8)&(lt IE 10)]>
		<script src="jquery-file-upload/js/cors/jquery.xdr-transport.js"></script>
		<![endif]-->

    	<div id="call_widget_container"></div>
    	<script src="js/call.js" charset="UTF-8"></script>

		<!-- /Yandex.Metrika counter -->
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-69297857-1', 'auto');
		  ga('send', 'pageview');

		</script>
	</body>
</html>


<?php return; ?>
<div class="addImages">
	<div class='block'>
		<h3>Добавить паспорт Генерального директора</h3>
		<div class="input-container">
            <label class="caption">Скан паспорта разворот с фото</label>
            <input type="file" name='skanGeneralnogo'>
        </div>

        <div class="input-container">
            <label class="caption">Скан паспорта разворот с пропиской</label>
            <input type="file" name='skanGeneralnogoSPropiskoj'>
        </div>
	</div>

	<div class='block'>
		<h3>Добавить паспорта учредителей</h3>
		<div style='display:none'>
			<div class='toClone additional'>
				<h4>Учредитель <span class='count'>1</span></h4>

				<div class="input-container">
		            <label class="caption">Скан паспорта разворот с фото</label>
		            <input type="file" name='skanUchreditelja1'>
		        </div>

		        <div class="input-container">
		            <label class="caption">Скан паспорта разворот с пропиской</label>
		            <input type="file" name='skanUchrediteljaCPropiskoj1'>
		        </div>
			</div>
		</div>
		

		<div class='additional'>
			<h4>Учредитель <span class='count'>1</span></h4>

			<div class="input-container">
	            <label class="caption">Скан паспорта разворот с фото</label>
	            <input type="file" name='skanUchreditelja1'>
	        </div>

	        <div class="input-container">
	            <label class="caption">Скан паспорта разворот с пропиской</label>
	            <input type="file" name='skanUchrediteljaCPropiskoj1'>
	        </div>
		</div>

		<div class="input-container">
			<button type='button' class="btn btn-red btn-small addAdditional">Добавить еще учредителя</button>
			<button type='button' class="btn btn-red btn-small deleteLastAdditional">Удалить последнего</button>
		</div>
		
	</div>
</div>